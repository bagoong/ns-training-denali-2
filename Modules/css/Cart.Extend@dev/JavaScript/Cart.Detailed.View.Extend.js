/*
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module Cart
define('Cart.Detailed.View.Extend'
,	[
	'Cart.Detailed.View'

	,	'jQuery'
	,	'Backbone'
	,	'underscore'
	,	'Utils'

	,	'jQuery.scStickyButton'

	]
,	function (
		cartView

	,	jQuery
	,	Backbone
	,	_
	)
{
	'use strict';
		// @method getContext @return {Cart.Detailed.View.Context}
		//

return _.extend(cartView.prototype,{


	updateItemQuantity:  function(e)
		{
			var self = this
			,	$line = null
			,	$form = jQuery(e.target).closest('form')
			,	options = $form.serializeObject()
			,	internalid = options.internalid
			,	line = this.model.get('lines').get(internalid)
			,	$input = $form.find('[name="quantity"]')
			,	validInput = this.validInputValue($input[0]);


			if (!line || this.isRemoving)
			{
				return;
			}

			if (!validInput || options.quantity)
			{
				var	new_quantity = parseInt(options.quantity, 10)
				,	item = line.get('item')
				,	min_quantity = item ? item.get('_minimumQuantity', true) : line.get('minimumquantity')
				,	current_quantity = parseInt(line.get('quantity'), 10);

				
				new_quantity = (new_quantity >= min_quantity) ? new_quantity : current_quantity;
				$input.val(new_quantity);
				this.storeColapsiblesState();


				if (new_quantity !==  current_quantity)
				{

					$line = this.$('#' + internalid);
					$input.val(new_quantity).prop('disabled', true);
					line.set('quantity', new_quantity);

					var invalid = line.validate();

					var customer;
					this.application.getUser().done(function(data){ customer=data });

					var loggedin = customer.get('isLoggedIn');
					

					if (loggedin == 'F')
					{
						
						multiplier = 2;

					}else{

						var multiplier = customer.get('custentity_quantity_multiplier');

						multiplier = multiplier ? parseInt(multiplier) : 2;
					
					}

					//From CSS training
					if (new_quantity % multiplier != 0)
					{	
					//	alert('wrong');
						this.showError(_('Items should be in multiple of 2.').translate());
						line.set('quantity', current_quantity);
				
					}	

					if (!invalid)
					{
						var update_promise = this.model.updateLine(line);

						this.disableElementsOnPromise(update_promise, 'article[id="' + internalid + '"] a, article[id="' + internalid + '"] button');

						update_promise.done(function ()
						{
							self.showContent().done(function (view)
							{
								view.resetColapsiblesState();
							});
						}).fail(function (jqXhr)
						{
							jqXhr.preventDefault = true;
							var result = JSON.parse(jqXhr.responseText);

							self.showError(result.errorMessage, $line, result.errorDetails);
							line.set('quantity', current_quantity);
						}).always(function ()
						{
							$input.prop('disabled', false);
						});
					}
					else
					{
						var placeholder = this.$('#' + internalid + ' [data-type="alert-placeholder"]');
						placeholder.empty();

						_.each(invalid, function(value)
						{
							var global_view_message = new GlobalViewsMessageView({
									message: value
								,	type: 'error'
								,	closable: true
							});

							placeholder.append(global_view_message.render().$el.html());
						});

						$input.prop('disabled', false);
						line.set('quantity', current_quantity);
					}
				}
			}
		}


	,	getContext: function ()
		{
			var lines = this.model.get('lines')
			,	product_count = lines.length
			,	item_count = _.reduce(lines.models, function(memo, line){ return memo + line.get('quantity'); }, 0)
			,	product_and_items_count = '';

			if (product_count === 1)
			{
				if (item_count === 1)
				{
					product_and_items_count =  _('1 Product, 1 Item').translate();
				}
				else
				{
					product_and_items_count = _('1 Product, $(0) Items').translate(item_count);
				}
			}
			else
			{
				if (item_count === 1)
				{
					product_and_items_count = _('$(0) Products, 1 Item').translate(product_count);
				}
				else
				{
					product_and_items_count = _('$(0) Products, $(1) Items').translate(product_count, item_count);
				}
			}

			// @class Cart.Detailed.View.Context
			return {

					//@property {Boolean} showLines
					showLines: !!(lines && lines.length)
					//@property {Orderline.Collection} lines
				,	lines: lines
					//@property {String} productsAndItemsCount
				,	productsAndItemsCount: product_and_items_count
					//@property {Number} productCount
				,	productCount: product_count
					//@property {Number} itemCount
				,	itemCount: item_count
					//@property {String} pageHeader
				,	pageHeader: this.page_header

				,	showmessage: true
			};
			// @class Cart.Detailed.View
		}
	});
});

/*
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

/* global customer */
// Profile.js
// ----------------
// This file define the functions to be used on profile service
define(
	'Profile.Model.Extend'
,	['Profile.Model'
	, 'Utils'
	, 'underscore'
	]
,	function (
	ProfileModel
	, Utils
	, _
	)
{
	'use strict';

	return _.extend(ProfileModel,{

	get: function ()
		{
			var profile = {};

			//Only can you get the profile information if you are logged in.
			if (session.isLoggedIn() && this.isSecure)
			{

				//Define the fields to be returned
				this.fields = this.fields || ['isperson', 'email', 'internalid', 'name', 'overduebalance', 'phoneinfo', 'companyname', 'firstname', 'lastname', 'middlename', 'emailsubscribe', 'campaignsubscriptions', 'paymentterms', 'creditlimit', 'balance', 'creditholdoverride'];

				profile = customer.getFieldValues(this.fields);

				//Make some attributes more friendly to the response
				profile.phone = profile.phoneinfo.phone;
				profile.altphone = profile.phoneinfo.altphone;
				profile.fax = profile.phoneinfo.fax;
				profile.priceLevel = (session.getShopperPriceLevel().internalid) ? session.getShopperPriceLevel().internalid : session.getSiteSettings(['defaultpricelevel']).defaultpricelevel;
				profile.type = profile.isperson ? 'INDIVIDUAL' : 'COMPANY';
				profile.isGuest = session.getCustomer().isGuest() ? 'T' : 'F';

				profile.creditlimit = parseFloat(profile.creditlimit || 0);
				profile.creditlimit_formatted = Utils.formatCurrency(profile.creditlimit);

				profile.balance = parseFloat(profile.balance || 0);
				profile.balance_formatted = Utils.formatCurrency(profile.balance);

				profile.balance_available = profile.creditlimit - profile.balance;
				profile.balance_available_formatted = Utils.formatCurrency(profile.balance_available);
			}
			else
			{
				profile = customer.getFieldValues();
				profile.subsidiary = session.getShopperSubsidiary();
				profile.language = session.getShopperLanguageLocale();
				profile.currency = session.getShopperCurrency();
				profile.isLoggedIn = Utils.isLoggedIn() ? 'T' : 'F';
				profile.isGuest = session.getCustomer().isGuest() ? 'T' : 'F';
				profile.priceLevel = session.getShopperPriceLevel().internalid ? session.getShopperPriceLevel().internalid : session.getSiteSettings('defaultpricelevel');

				profile.internalid = nlapiGetUser() + '';

				
			}
				var customValues = customer.getCustomFieldValues();
				_.each(customValues, function (customValue){
				profile[customValue.name] = customValue.value;
				});

			return profile;
		}

	});
});

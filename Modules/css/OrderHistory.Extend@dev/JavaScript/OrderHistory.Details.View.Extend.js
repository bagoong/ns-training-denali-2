/*
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

//@module OrderHistory
define('OrderHistory.Details.View.Extend'
,	[
		'OrderHistory.Details.View'

	,	'order_history_details.tpl'

	,	'Backbone'
	,	'underscore'
	,	'jQuery'
	,	'Tracker'
	,	'LiveOrder.Model'
	]
,	function (
		OrderHistoryDetailsView

	,	order_history_details_tpl

	,	Backbone
	,	_
	,	jQuery
	,	Tracker
	,	LiveOrderModel
	)
{
	'use strict';

	return _.extend(OrderHistoryDetailsView.prototype,{

		//@method getContext @returns OrderHistory.Details.View.Context
	getContext: function()
		{
			var lines = this.model.get('lines')
			,	self = this
			,	return_authorizations = this.getReturnAuthorizations()
			,	any_line_purchasable = lines.any(function (line)
				{
					return line.get('item').get('_isPurchasable');
				})
			,	all_gift_certificates = lines.all(function (line)
				{
					return line.get('item').get('itemtype') === 'GiftCert';
				})
			,	non_shippable_items = this.getNonShippableItems()
			,	unfulfilled = this.getUnfulfilled()
			,	is_basic = self.application.getConfig('isBasic')
			,	payment_transactions = this.model.get('receipts').map(function (record)
				{
					var is_invoice = record.get('recordtype') === 'CustInvc'
					,	hide_link = is_basic && is_invoice
					,	link = '';

					if (is_invoice)
					{
						link = is_basic ? undefined : ('invoices/' + record.get('internalid'));
					}
					else
					{
						link = '/receiptshistory/view/' + record.get('internalid');
					}


					record.set('showLink', !hide_link);
					record.set('link', link);
					record.set('isInvoice', is_invoice);
					record.set('dataType', is_invoice && !is_basic ? 'invoice' : 'receipt');

					return record;
				})
			,	order_ship_method = this.model.get('shipmethod')
			,	delivery_method_name = '';


			if (order_ship_method && this.model.get('shipmethods')._byId[order_ship_method])
			{
				delivery_method_name = this.model.get('shipmethods')._byId[order_ship_method].getFormattedShipmethod();
			}
			else if (order_ship_method && order_ship_method.name)
			{
				delivery_method_name = order_ship_method.name;
			}
			//@class OrderHistory.Details.View.Context
			return {
					//@property {OrderHistory.Model} model
					model : this.model
					//@property {Boolean} showNonShippableItems
				,	showNonShippableItems: !!non_shippable_items.length
					//@property {Array} nonShippableItems
				,	nonShippableItems: non_shippable_items
					//@property {Boolean} nonShippableItemsLengthGreaterThan1
				,	nonShippableItemsLengthGreaterThan1: non_shippable_items.length > 1
					//@property {Array} fulfillmentAddresses
				,	fulfillmentAddresses: this.getFulfillmentAddresses()
					//@property {Array} unfulfilledItems
				,	unfulfilledItems: unfulfilled
					//@property {Boolean} showUnfulfilledItems
				,	showUnfulfilledItems: !!unfulfilled.length
					//@property {Boolean} unfulfilledItemsLengthGreaterThan1
				,	unfulfilledItemsLengthGreaterThan1: unfulfilled.length > 1
					//@property {OrderLine.Collection} lines
				,	lines: lines
					//@property {Boolean} collapseElements
				,	collapseElements: this.application.getConfig('collapseElements')
					//@property {String} pdfUrl
				,	pdfUrl: _.getDownloadPdfUrl({
						asset: 'order-details'
					,	id: this.model.get('internalid')
					})
					//@property {Address.Model} billAddress
				,	billAddress: this.model.get('billaddress') ? this.model.get('addresses').get(this.model.get('billaddress'))  : null
					//@property {Boolean} showOrderShipAddress
				,	showOrderShipAddress: !!this.model.get('shipaddress')
					//@property {Address.Model} orderShipaddress
				,	orderShipaddress: this.model.get('shipaddress') ? this.model.get('addresses').get(this.model.get('shipaddress')) : null
					//@property {Boolean} showReturnAuthorizations
				,	showReturnAuthorizations: !!return_authorizations.length
					//@property {Object} returnAuthorizations
				,	returnAuthorizations: return_authorizations
					//@property {Boolean} çshowReorderAllItemsButton
				,	showReorderAllItemsButton: any_line_purchasable && !all_gift_certificates
					//@property {Boolean} showRequestReturnButton
				,	showRequestReturnButton: !!this.isReturnable()
					//@property {Boolean} showPaymentTransactions
				,	showPaymentTransactions: !!payment_transactions.length
					//@property {Boolean} paymentTransactions
				,	paymentTransactions: payment_transactions
					//@property {Array} showSummaryDiscount
				,	showSummaryDiscount: !!this.model.get('summary').discounttotal
					//@property {Boolean} showSummaryShippingCost
				,	showSummaryShippingCost: !!this.model.get('summary').shippingcost
					//@property {Boolean} showSummaryHandlingCost
				,	showSummaryHandlingCost: !!this.model.get('summary').handlingcost
					//@property {Boolean} showSummaryGiftCertificateTotal
				,	showSummaryGiftCertificateTotal: !!this.model.get('summary').giftcertapplied
					//@property {Boolean} showSummaryPromocode
				,	showSummaryPromocode: !!this.model.get('promocode')
					//@property {String} deliveryMethodName
				,	deliveryMethodName: delivery_method_name || ''
					//@property {Boolean} showDeliveryMethod
				,	showDeliveryMethod: !!delivery_method_name

				,	showshippingnotes: this.model.get('comments')

			};
		}

	});

});

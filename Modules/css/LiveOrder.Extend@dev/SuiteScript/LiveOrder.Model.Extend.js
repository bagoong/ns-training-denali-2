/*
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

/* jshint -W053 */
// We HAVE to use "new String"
// So we (disable the warning)[https://groups.google.com/forum/#!msg/jshint/O-vDyhVJgq4/hgttl3ozZscJ]
/* global order,customer */
// @module LiveOrder
define(
	'LiveOrder.Model.Extend'
,	[	
		'LiveOrder.Model'
	, 	'SC.Model'
	,	'Application'
	,	'Profile.Model'
	,	'StoreItem.Model'
	,	'Utils'
	,	'underscore'
	]
,	function (
		LiveOrderModel
	,	SCModel
	,	Application
	,	Profile
	,	StoreItem
	,	Utils
	,	_
	)
{
	'use strict';

		return _.extend(LiveOrderModel,{

	getShipMethods: function (order_fields)
		{
			var profile = Profile.get();

			console.log('profile', JSON.stringify(profile));

			cust_shipmethod = profile.custentity_css_shipping;

			order_fields.shipmethods = _.filter(order_fields.shipmethods,{shipmethod : cust_shipmethod});

			var shipmethods = _.map(order_fields.shipmethods, function (shipmethod)

			{
				var rate = Utils.toCurrency(shipmethod.rate.replace( /^\D+/g, '')) || 0;	
				return {
					internalid: shipmethod.shipmethod
				,	name: shipmethod.name
				,	shipcarrier: shipmethod.shipcarrier
				,	rate: rate
				,	rate_formatted: shipmethod.rate
				};
				
			});

			return shipmethods;
		}

	});
});

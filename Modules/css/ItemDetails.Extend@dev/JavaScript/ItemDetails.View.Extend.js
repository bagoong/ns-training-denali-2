/*
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

//@module ItemDetails
define(
	'ItemDetails.View.Extend'
,	[
		'ItemDetails.View'
	
	,	'SC.Configuration'
	,	'Backbone'
	,	'underscore'
	,	'jQuery'
	,	'RecentlyViewedItems.Collection'
	,	'LiveOrder.Model'

	]
,	function (
		ItemDetailsView

	,	Configuration
	,	Backbone
	,	_
	,	jQuery
	,	RecentlyViewedItemsCollection
	,	LiveOrderModel
	)
{
	'use strict';

	var colapsibles_states = {};

	//@class ItemDetails.View Handles the PDP and quick view @extend Backbone.View

	return _.extend(ItemDetailsView.prototype,{

		// @method addToCart Updates the Cart to include the current model
		// also takes care of updating the cart if the current model is already in the cart
		// @param {jQuery.Event} e
		//@return {Void}
	addToCart: function (e)
		{
			e.preventDefault();

			var customer;
			this.application.getUser().done(function(data){ customer=data });

			var loggedin = customer.get('isLoggedIn');
			

			if (loggedin == 'F')
			{
				
				multiplier = 2;

			}else{

				var multiplier = customer.get('custentity_quantity_multiplier')

				multiplier = multiplier ? parseInt(multiplier) : 2;
			
			}


			// Updates the quantity of the model
			var quantity = this.$('[name="quantity"]').val();
			this.model.setOption('quantity', quantity);

			if(quantity % multiplier != 0)
			{
				this.showError(_('Items should be in multiple of 2.').translate());
				return;
			}	

			if (this.model.isValid(true) && this.model.isReadyForCart())
			{
				var self = this
				,	cart = LiveOrderModel.getInstance()
				,	layout = this.application.getLayout()
				,	cart_promise
				,	error_message = _('Sorry, there is a problem with this Item and can not be purchased at this time. Please check back later.').translate()
				,	isOptimistic = this.application.getConfig('addToCartBehavior') === 'showCartConfirmationModal';

				if (this.model.itemOptions && this.model.itemOptions.GIFTCERTRECIPIENTEMAIL)
				{
					if (!Backbone.Validation.patterns.email.test(this.model.itemOptions.GIFTCERTRECIPIENTEMAIL.label))
					{
						self.showError(_('Recipient email is invalid').translate());
						return;
					}
				}

				if (isOptimistic)
				{
					//Set the cart to use optimistic when using add to cart
					// TODO pass a param to the add to cart instead of using this hack!
					cart.optimistic = {
						item: this.model
					,	quantity: quantity
					};
				}

				// If the item is already in the cart
				if (this.model.cartItemId)
				{
					cart_promise = cart.updateItem(this.model.cartItemId, this.model).done(function ()
					{
						//If there is an item added into the cart
						if (cart.getLatestAddition())
						{
							if (self.$containerModal)
							{
								self.$containerModal.modal('hide');
							}

							if (layout.currentView instanceof require('Cart.Detailed.View'))
							{
								layout.currentView.showContent();
							}
						}
						else
						{
							self.showError(error_message);
						}
					});
				}
				else
				{
					cart_promise = cart.addItem(this.model).done(function ()
					{
						//If there is an item added into the cart
						if (cart.getLatestAddition())
						{
							if (!isOptimistic)
							{
								layout.showCartConfirmation();
							}
						}
						else
						{
							self.showError(error_message);
						}
					});

					if (isOptimistic)
					{
						cart.optimistic.promise = cart_promise;
						layout.showCartConfirmation();
					}
				}

				// Handle errors. Checks for rollback items.
				cart_promise.fail(function (jqXhr)
				{
					var error_details = null;
					try
					{
						var response = JSON.parse(jqXhr.responseText);
						error_details = response.errorDetails;
					}
					finally
					{
						if (error_details && error_details.status === 'LINE_ROLLBACK')
						{
							var new_line_id = error_details.newLineId;
							self.model.cartItemId = new_line_id;
						}

						self.showError(_('We couldn\'t process your item').translate());

						if (isOptimistic)
						{
							self.showErrorInModal(_('We couldn\'t process your item').translate());
						}
					}
				});

				// Disables the button while it's being saved then enables it back again
				if (e && e.target)
				{
					var $target = jQuery(e.target).attr('disabled', true);

					cart_promise.always(function ()
					{
						$target.attr('disabled', false);
					});
				}
			}

		// @method refreshInterface
		// Computes and store the current state of the item and refresh the whole view, re-rendering the view at the end
		// This also updates the URL, but it does not generates a history point
		//@return {Void}
	
		}
	});
});

//@class ItemDetails.View.Initialize.Parameters
//@property {ItemDetails.Model} model
//@property {String} baseUrl
//@property {ApplicationSkeleton} application

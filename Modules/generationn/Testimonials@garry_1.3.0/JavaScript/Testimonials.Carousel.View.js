define('Testimonials.Carousel.View'
, [
    'Backbone'
  , 'testimonials_carousel.tpl'
  ]
, function TestimonialsCarouselView(
    Backbone
  , testimonialsCarouselTpl
  )
{
  'use strict';

  return Backbone.View.extend({
    template: testimonialsCarouselTpl
  , initialize: function initialize(){
      console.log('Carousel initialized');
    }
  })
});
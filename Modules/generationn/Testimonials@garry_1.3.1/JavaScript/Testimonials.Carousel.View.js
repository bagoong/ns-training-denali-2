define('Testimonials.Carousel.View'
, [
    'Backbone'
  , 'testimonials_carousel.tpl'
  ]
, function TestimonialsCarouselView(
    Backbone
  , testimonialsCarouselTpl
  )
{
  'use strict';

  return Backbone.View.extend({
    template: testimonialsCarouselTpl
  , initialize: function initialize(){
    //  console.log('Carousel initialized');
      this.collectionPromise = this.collection.fetch();
    }

  , render: function render() {
    var self = this;
    this.collectionPromise.done(function doneCollection() {
      self._render();
    });
  }

  , getContext: function getContext() {
      return {
        content: JSON.stringify(this.collection.toJSON())
      };
    }  
  })
});